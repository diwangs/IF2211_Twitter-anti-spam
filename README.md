# Anti-Spam
Knuth-Morris-Pratt, Boyer-Moore Algorithm, and Regex pattern matching for post-spam detection in Twitter API

## Author
* Senapati Sang Diwangkara
* [Albert Sahala](https://github.com/albertsahala)
* Nicolaus Boby
